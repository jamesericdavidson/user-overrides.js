// Google Safe Browsing (0401, 0402, 0403, 0404, 0405): disabled
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.allowOverride", false);

// DNS-over-HTTPS (0710): disabled
user_pref("network.trr.mode", 5);

// Search bar (0801): enabled
user_pref("keyword.enabled", true);

// Colour visited links (0820): disabled
user_pref("layout.css.visited_links_enabled", false);

// Media autoplay (2030): video and audio disabled
user_pref("media.autoplay.default", 5);
user_pref("media.autoplay.blocking_policy", 2);

// Memory cache (5002): disabled
user_pref("browser.cache.memory.enable", false);
user_pref("browser.cache.memory.capacity", 0);

// Ask to save passwords (5003): disabled
user_pref("signon.rememberSignons", false);

// Certificate caching (5005): disabled
user_pref("security.nocertdb", true);

// Undo closed tabs (5007): disabled
user_pref("browser.sessionstore.max_tabs_undo", 0);

// Location bar suggestions (5010): disabled (excluding bookmarks)
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.suggest.topsites", false);

// "Open with" download prompt (5009): disabled
user_pref("browser.download.forbid_open_with", true);

// Save downloads to another directory (5016): enabled
user_pref("browser.download.folderList", 2);

// Autofill forms (5017): disabled
user_pref("extensions.formautofill.addresses.enabled", false);
user_pref("extensions.formautofill.creditCards.enabled", false);
user_pref("extensions.formautofill.heuristics.enabled", false);

// Pop-up events (5018): reduced
user_pref("dom.popup_allowed_events", "click dblclick mousedown pointerdown");

// Create screenshots of visited pages (5019): disabled
user_pref("browser.pagethumbnails.capturing_disabled", true);

// Disable unwanted features
user_pref("extensions.pocket.enabled", false);

// Enable hardware acceleration / video decoding
user_pref("media.ffmpeg.vaapi.enabled", true);
